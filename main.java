import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Main {
  public static void main(String[] args) {
    double pd = 1.0/3.0; //probab. of defect
    double pnd = 2.0/3.0; //probab. of the contrary
    //what is the probab the the 1st defect occurs during the first 5 trials?
    double p = pd*(1.0+pnd+pnd*pnd+Math.pow(pnd,3)+Math.pow(pnd,4));
    p = Math.round(1000.0*p)/1000.0;
    System.out.println(p);
  }
}
